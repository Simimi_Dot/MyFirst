//
//  MyFirstApp.swift
//  MyFirst
//
//  Created by Егор Астахов on 01.08.2023.
//

import SwiftUI

@main
struct MyFirstApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
